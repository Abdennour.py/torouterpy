# Torouterpy

Tor Router allow you to use TOR as a transparent proxy and send all your trafic under TOR **INCLUDING DNS REQUESTS**
This version of Tor Router use Python for automation


# Instalation :

**It script require root privileges**
1. Open a terminal and clone the script using the following command:
```
→ git clone https://gitlab.com/Abdennour.py/torouterpy.git
→ cd torouterpy
→ pip install -r requirements.txt
→ chmod +x src/tor-router
→ chmod +x *
→ ./torouter.py
```
